define(["knockout", "text!./home.html"], function (ko, homeTemplate) {


    function HomeViewModel(route) {
        var self = this;

        this.message = ko.observable('Welcome to EX Store!');
    }

    HomeViewModel.prototype.doSomething = function () {
        this.message('You invoked doSomething() on the viewmodel.');
    };

    return {viewModel: HomeViewModel, template: homeTemplate};

});
