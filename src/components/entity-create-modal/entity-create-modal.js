define(['knockout', 'text!./entity-create-modal.html'], function (ko, templateMarkup) {

    ko.bindingHandlers.modal = {
        init: function (element, valueAccessor) {
            $(element).modal({
                show: false
            });

            var value = valueAccessor();
            if (typeof value === 'function') {
                $(element).on('hide.bs.modal', function () {
                    value(false);
                });
            }
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $(element).modal("destroy");
            });

        },
        update: function (element, valueAccessor) {
            var value = valueAccessor();
            if (ko.utils.unwrapObservable(value)) {
                $(element).modal('show');
            } else {
                $(element).modal('hide');
            }
        }
    }

    var RecordLine = function () {
        var self = this;

        self.inputKey = ko.observable();
        self.inputValue = ko.observable();
    };

    function EntityCreateModal(params) {
        var self = this;

        self.showDialog = params.showDialog;
        self.entityPage = params.entityPage;

        self.lines = ko.observableArray([
            new RecordLine(),
            new RecordLine()
        ]);

        self.addLine = function () {
            self.lines.push(new RecordLine())
        };

        self.removeLine = function (line) {
            self.lines.remove(line)
        };

        self.submit = function () {
            self.showDialog(false);
            var records = self.lines();
            self.lines([]);
            self.addLine();
            if (records) {
                var list = [];
                for (var i = 0; i < records.length; ++i) {
                    if (records[i].inputKey() && records[i].inputValue()) {
                        list.push({
                            key: records[i].inputKey(),
                            value: records[i].inputValue()
                        });
                    }
                }
                self.entityPage.addRecords(list);
            }
        }

    }

    // This runs when the component is torn down. Put here any logic necessary to clean up,
    // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
    EntityCreateModal.prototype.dispose = function () {
    };

    return {viewModel: EntityCreateModal, template: templateMarkup};

});
