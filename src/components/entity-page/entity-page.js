define(['knockout', 'text!./entity-page.html'], function (ko, templateMarkup) {

    function EntityPage(params) {
        var self = this;

        var delegate = new GapiDelegate(function () {
            delegate.loadData(self.loadDataCallback);
        });

        // lol
        self.entityPage = self;
        //~
        self.queryId = ko.observable();
        self.queryKey = ko.observable();
        self.items = ko.observable();
        self.showDialog = ko.observable(false);
        self.showModifyDialog = ko.observable(false);
        self.selectedItem = ko.observable();

        self.loadDataCallback = {
            success: function (data) {
                self.items(data);
            },
            error: function (error) {
                alert(error.code + " Reason: " + error.message);
            }
        }

        self.addRecords = function(list) {
            delegate.create(list, self.loadDataCallback);
        }

        self.updateRecord = function(record) {
            delegate.updateRecord(record, self.loadDataCallback);
        }

        self.deleteRecord = function(record) {
            delegate.deleteById(record, self.loadDataCallback);
        }

        self.onApplyClick = function () {
            if (self.queryId() && self.queryId() != '') {
                delegate.loadById(self.queryId(), self.loadDataCallback);
            } else if (self.queryKey() && self.queryKey() != '') {
                delegate.loadByKey(self.queryKey(), self.loadDataCallback);
            } else {
                delegate.loadData(self.loadDataCallback)
            }
        }

        self.onAddClick = function () {
            self.showDialog(true);
        }

        self.onItemClick = function(item) {
            self.selectedItem(item);
            self.showModifyDialog(true);
        }

    }

    EntityPage.prototype.dispose = function () {
    }

    return {viewModel: EntityPage, template: templateMarkup};

});
