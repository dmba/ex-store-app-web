define(['knockout', 'text!./entity-modify-modal.html'], function (ko, templateMarkup) {

    ko.bindingHandlers.modal = {
        init: function (element, valueAccessor) {
            $(element).modal({
                show: false
            });

            var value = valueAccessor();
            if (typeof value === 'function') {
                $(element).on('hide.bs.modal', function () {
                    value(false);
                });
            }
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $(element).modal("destroy");
            });

        },
        update: function (element, valueAccessor) {
            var value = valueAccessor();
            if (ko.utils.unwrapObservable(value)) {
                $(element).modal('show');
            } else {
                $(element).modal('hide');
            }
        }
    }

    function EntityModifyModal(params) {
        var self = this;

        self.showModifyDialog = params.showModifyDialog;
        self.entityPage = params.entityPage;

        self.inputKey = ko.observable();
        self.inputValue = ko.observable();
        self.item = {};

        params.selectedItem.subscribe(function (item) {
            self.item = item;
            self.inputKey(item.key);
            self.inputValue(item.value);
        });

        self.onSubmitClick = function () {
            self.showModifyDialog(false);
            if(self.isItemChanged()) {
                self.item.key = self.inputKey();
                self.item.value = self.inputValue();
                self.entityPage.updateRecord(self.item);
            }
        };

        self.onCancelClick = function () {
            self.showModifyDialog(false);
        };

        self.onDeleteClick = function () {
            self.showModifyDialog(false);
            self.entityPage.deleteRecord(self.item);
        };

        self.isItemChanged = function() {
            return self.item.key != self.inputKey() || self.item.value != self.inputValue();
        }
    }

    EntityModifyModal.prototype.dispose = function () {
    };

    return {viewModel: EntityModifyModal, template: templateMarkup};

});
