define(['jquery', 'knockout', './router', 'bootstrap', 'knockout-projections'], function($, ko, router) {

  // Components can be packaged as AMD modules, such as the following:
  ko.components.register('nav-bar', { require: 'components/nav-bar/nav-bar' });

  ko.components.register('home-page', { require: 'components/home-page/home' });

  ko.components.register('entity-page', { require: 'components/entity-page/entity-page' });

  ko.components.register('entity-create-modal', { require: 'components/entity-create-modal/entity-create-modal' });

  ko.components.register('entity-modify-modal', { require: 'components/entity-modify-modal/entity-modify-modal' });

  // [Scaffolded component registrations will be inserted here. To retain this feature, don't remove this comment.]

  // Start the application
  ko.applyBindings({ route: router.currentRoute });

});
