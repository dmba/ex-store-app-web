/**
 * Created by dmba on 10/14/15.
 */
function GapiDelegate(loadCallback) {

    var self = this;
    var apiName = 'store';
    var apiVersion = 'v1';
    var apiRoot = 'https://ex-univ-store-api.appspot.com/_ah/api';

    self.init = function (loadCallback) {
        gapi.client.load(apiName, apiVersion, loadCallback, apiRoot);
    }

    self.loadData = function (callback) {
        gapi.client.store.data.get({'name': 'param'}).execute(
            function (response) {
                if (callback && response.result == undefined) {
                    callback.error(response.error);
                    return;
                }
                var items = response.result.items;
                var data = [];
                for (var i in items) {
                    data.push({
                        id: items[i].id ? items[i].id : '',
                        modified: self.formatDate(items[i].modified ? items[i].modified : ''),
                        key: items[i].key ? items[i].key : '',
                        value: items[i].value ? items[i].value : ''
                    });
                }
                if (callback) {
                    callback.success(data);
                }
            }
        );
    }

    self.loadById = function (id, callback) {
        gapi.client.store.data.getById({'id': id}).execute(
            function (response) {
                if (callback && response.result == undefined) {
                    callback.error(response.error);
                    return;
                }
                var item = response.result;
                var data = [{
                    id: item.id,
                    modified: self.formatDate(item.modified),
                    key: item.key,
                    value: item.value
                }];
                if (callback) {
                    callback.success(data);
                }
            }
        );
    }

    self.loadByKey = function (key, callback) {
        gapi.client.store.data.get({'key': key}).execute(
            function (response) {
                if (callback && response.result == undefined) {
                    callback.error(response.error);
                    return;
                }
                var item = response.result;
                var data = [{
                    id: item.id,
                    modified: self.formatDate(item.id),
                    key: item.key,
                    value: item.value
                }];
                if (callback) {
                    callback.success(data);
                }
            }
        );
    }

    self.create = function (list, callback) {
        for (var i in list) {
            list[i].modified = undefined;
        }
        var request = {
            data: list
        };
        gapi.client.store.data.post(request).execute(
            function (response) {
                if (callback && response.result == undefined) {
                    callback.error(response.error);
                    return;
                }
                var items = response.result.items;
                var data = [];
                for (var i in items) {
                    data.push({
                        id: items[i].id ? items[i].id : '',
                        modified: self.formatDate(items[i].modified ? items[i].modified : ''),
                        key: items[i].key ? items[i].key : '',
                        value: items[i].value ? items[i].value : ''
                    });
                }
                if (callback) {
                    callback.success(data);
                }
            }
        );
    }

    self.updateRecord = function (record, callback) {
        self.create([record], callback);
    }

    self.deleteById = function (record, callback) {
        gapi.client.store.data.delete({'id': record.id}).execute(
            function (response) {
                self.loadData(callback);
            }
        );
    }

    self.formatDate = function (date) {
        return new Date(date).toLocaleString();
    }

    $(window).load(function () {
        self.init(loadCallback);
    });

}